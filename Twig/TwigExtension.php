<?php

declare(strict_types=1);

namespace Doudou\BaseBundle\Twig;

use Doudou\BaseBundle\Helper\DateTimeHelper;
use DateTime;
use DateTimeInterface;
use Exception;
use Doudou\BaseBundle\Contract\Manager\PaginatorInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{
    /**
     * @var array
     */
    private $displayAppVersion;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var array
     */
    private $paginationSettings;
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * TwigExtension constructor.
     * @param TranslatorInterface $translator
     * @param array $displayAppVersion
     * @param array $paginationSettings
     * @param UrlGeneratorInterface $router
     */
    public function __construct(
        TranslatorInterface $translator,
        array $displayAppVersion,
        array $paginationSettings,
        UrlGeneratorInterface $router
    ) {
        $this->translator = $translator;
        $this->displayAppVersion = $displayAppVersion;
        $this->paginationSettings = $paginationSettings;
        $this->router = $router;
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions()
    {
        return array(
            new TwigFunction('render_date', array($this, 'renderDate')),
            new TwigFunction('render_time', array($this, 'renderTime')),
            new TwigFunction('render_date_and_time', array($this, 'renderDateAndTime')),
            new TwigFunction('has_active_link', array($this, 'hasActiveLink')),
            new TwigFunction('display_front_app_version', array($this, 'displayFrontAppVersion')),
            new TwigFunction('display_back_app_version', array($this, 'displayBackendAppVersion')),
            new TwigFunction('render_limitator_filter',
                array($this, 'getLimitator'),
                array('needs_environment' => true, 'is_safe' => ['html'])
            )
        );
    }

    /**
     * @return array|TwigFilter[]
     */
    public function getFilters(): array
    {
        return array(
            new TwigFilter('time_diff', array($this, 'timeDiff'), array(
                'needs_environment' => true
            )),
            new TwigFilter('day_diff', array($this, 'dayDiff'), array(
                'needs_environment' => true
            ))
        );
    }

    /**
     * @param DateTimeInterface|null $date
     * @param null|string $displayEmpty
     * @return string
     */
    public function renderDate(?DateTimeInterface $date, ?string $displayEmpty = null): string
    {
        if ($date === null) {
            if ($displayEmpty !== null) {
                return $displayEmpty;
            }
            return '';
        }
        return DateTimeHelper::formatDate($date);
    }

    /**
     * @param DateTimeInterface|null $dateTime
     * @param null|string $displayEmpty
     * @return string
     */
    public function renderTime(?DateTimeInterface $dateTime, ?string $displayEmpty = null): string
    {
        if ($dateTime === null) {
            if ($displayEmpty !== null) {
                return $displayEmpty;
            }
            return '';
        }
        return DateTimeHelper::formatTime($dateTime);
    }

    /**
     * @param DateTimeInterface|null $date
     * @param null|string $displayEmpty
     * @return string
     */
    public function renderDateAndTime(?DateTimeInterface $date, ?string $displayEmpty = null): string
    {
        if ($date === null) {
            if ($displayEmpty !== null) {
                return $displayEmpty;
            }
            return '';
        }
        return $date->format('d/m/Y à H:i');
    }

    /**
     * @param array $links [{isActive: ?bool}]
     * @return bool
     */
    public function hasActiveLink(array $links): bool
    {
        $hasActiveLink = false;
        foreach ($links as $link) {
            $hasActiveLink = ((isset($link['isActive']) && $link['isActive'] === true) ? true : $hasActiveLink);
            if ($hasActiveLink === true) {
                break;
            }
        }
        return $hasActiveLink;
    }

    /**
     * @return bool
     */
    public function displayFrontAppVersion(): bool
    {
        return $this->displayAppVersion['front'];
    }

    /**
     * @return bool
     */
    public function displayBackendAppVersion(): bool
    {
        return $this->displayAppVersion['backoffice'];
    }

    /**
     * @param Environment $env
     * @param DateTimeInterface|null $date
     * @param DateTimeInterface|null $now
     * @param bool $withPrefix
     * @return string
     * @throws Exception
     */
    public function timeDiff(
        Environment $env,
        ?DateTimeInterface $date,
        ?DateTimeInterface $now = null,
        bool $withPrefix = true
    ): string {
        if ($date === null) {
            return '';
        }
        $now = twig_date_converter($env, $now ?: new DateTime());
        $date = twig_date_converter($env, $date);
        $diff = $now->diff($date);
        $val = null;


        $days = $diff->days;
        if ($days === 0) {
            if ($diff->h !== 0) {
                $val =
                    $this->translator->transChoice('date_diff.hour', $diff->h) .
                    $this->translator->trans('date_diff.and') .
                    $this->translator->transChoice('date_diff.minute', $diff->i);
            } else {
                if ($diff->i !== 0) {
                    $val =
                        $this->translator->transChoice('date_diff.minute', $diff->i) .
                        $this->translator->trans('date_diff.and');
                }
                $val .= $this->translator->transChoice('date_diff.second', $diff->s);
            }
        } else {
            if ($diff->y !== 0) {
                $val =
                    $this->translator->transChoice('date_diff.year', $diff->y) .
                    $this->translator->trans('date_diff.and') .
                    $this->translator->transChoice('date_diff.month', $diff->m);
            } else {
                if ($diff->m !== 0) {
                    $val =
                        $this->translator->transChoice('date_diff.month', $diff->m) .
                        $this->translator->trans('date_diff.and');
                }
                $val .= $this->translator->transChoice('date_diff.day', $diff->d);
            }
        }
        if ($withPrefix === true) {
            $val = ($diff->invert === 0 ? $this->translator->trans('date_diff.in')
                    : $this->translator->trans('date_diff.ago')) . $val;
        }
        return $val;
    }

    /**
     * @param Environment $env
     * @param DateTimeInterface|null $date
     * @param DateTimeInterface|null $now
     * @param bool $withPrefix
     * @return string
     * @throws Exception
     */
    public function dayDiff(
        Environment $env,
        ?DateTimeInterface $date,
        ?DateTimeInterface $now = null,
        bool $withPrefix = true
    ): string {
        if ($date === null) {
            return '';
        }
        $now = twig_date_converter($env, $now ?: new DateTime());
        $date = twig_date_converter($env, $date);
        $diff = $now->diff($date);
        $val = null;
        if ($diff->days !== 0) {
            $val = $this->translator->transChoice('date_diff.day', $diff->days);
        } else {
            return $this->translator->trans('date_diff.today');
        }
        if ($withPrefix === true) {
            $val = ($diff->invert === 0 ? $this->translator->trans('date_diff.in')
                    : $this->translator->trans('date_diff.ago')) . $val;
        }
        return $val;
    }

    /**
     * @param DateTimeInterface $dateTime
     * @param DateTimeInterface|null $to
     * @return string
     * @throws Exception
     */
    public function formatTimeDiff(DateTimeInterface $dateTime, ?DateTimeInterface $to = null): string
    {
        if ($to === null) {
            $to = new DateTime();
        }

        $diff = $to->diff($dateTime);
        return $diff->format('%R%a jours');
    }

    /**
     * @param Environment $environment
     * @param PaginatorInterface $items
     * @param array $options
     * @param bool|null $isActive
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function getLimitator(Environment $environment, PaginatorInterface $items, array $options, ?bool $isActive = false): string
    {
        $landings = $this->paginationSettings['landings'];
        $defaultLanding = $this->paginationSettings['default_landing'];
        if (array_key_exists('landings', $options) && $options['landings'] !== null) {
            $landings = $options['landings'];
        }
        if (array_key_exists('defaultLanding', $options) && $options['defaultLanding'] !== null) {
            $defaultLanding = $options['defaultLanding'];
        }
        $routeName = $options['routeName'];
        $routeParams = $options['routeParams'];
        $currentLanding = (int)$defaultLanding;
        if (array_key_exists('limit', $routeParams)) {
            $currentLanding = (int)$routeParams['limit'];
        }
        if(isset($routeParams['page'])) {
            $routeParams['page'] = 1;
        }
        if (count($items) < $landings[0]  || $isActive === false) {
            return $environment->render(
                'shared/component/paginator/_paginator_items.html.twig',
                array(
                'items' => $items
                )
            );
        }
        return $environment->render(
            'shared/component/paginator/_paginator_limitator.html.twig',
            array(
            'nb' => count($items),
            'current_landing' => $currentLanding,
            'landings' => $this->generateLandings($landings, $routeName, $routeParams, $currentLanding),
            )
        );
    }

    /**
     * @param array $landings
     * @param string $routeName
     * @param array $routeParams
     * @param int $currentLanding
     * @return array
     */
    private function generateLandings(
        array $landings,
        string $routeName,
        array $routeParams,
        int $currentLanding
    ): array {
        $landingsObject = array();
        $dropDownClass = "dropdown-item";
        foreach ($landings as $landing) {
            $dropDownClass = "dropdown-item";
            $landingObject = array();
            $thisRouteParams = $routeParams;
            $thisRouteParams['limit'] = $landing;
            if ($currentLanding === $landing) {
                $dropDownClass = "dropdown-item active";
            }
            $landingObject['class'] = $dropDownClass;
            $landingObject['route'] = $this->router->generate($routeName, $thisRouteParams, 0);
            $landingObject['limit'] = $landing;
            $landingsObject[] = $landingObject;
        }
        return $landingsObject;
    }
}
