<?php

declare(strict_types=1);

namespace Doudou\BaseBundle\Form\Field;

/**
 * Class DateTimeToCheckBoxFieldType
 * @package Doudou\BaseBundle\Form\Field
 */
class DateTimeImmutableField extends AbstractDateTimeToImmutable
{
    /**
     * @return string
     */
    public function getParent(): string
    {
        return DatePickerType::class;
    }
}
