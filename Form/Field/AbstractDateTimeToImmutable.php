<?php

declare(strict_types=1);

namespace Doudou\BaseBundle\Form\Field;

use DateTime;
use DateTimeImmutable;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DateTimeToCheckBoxFieldType
 * @package Doudou\BaseBundle\Form\Field
 */
abstract class AbstractDateTimeToImmutable extends AbstractType
{
    /**
     * @return string
     */
    public function getParent(): string
    {
        return DateTimeType::class;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->addModelTransformer(new CallbackTransformer(
            static function (?DateTimeImmutable $immutableToDateTime) {
                if ($immutableToDateTime === null) {
                    return null;
                }
                return DateTime::createFromImmutable($immutableToDateTime);
            },
            static function (?DateTime $dateTimeToImmutable) {
                if ($dateTimeToImmutable === null) {
                    return null;
                }
                return DateTimeImmutable::createFromMutable($dateTimeToImmutable);
            }
        ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(array());
    }
}
