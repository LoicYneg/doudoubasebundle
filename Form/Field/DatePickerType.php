<?php

namespace Doudou\BaseBundle\Form\Field;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DatePickerType extends AbstractType
{
    /**
     * @var string|null
     */
    private $locale;

    public function __construct(?string $locale = null)
    {
        $this->locale = $locale;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        if ($this->locale === "fr" || $this->locale === null) {
            $format = "dd/MM/yyyy";
        } else {
            $format = "yyyy/MM/dd";
        }
        $resolver->setDefaults(array(
            'widget' => "single_text",
            'input' => 'datetime',
            'format' => $format,
            'required' => false,
            'attr' => array(
                'autocomplete' => 'off'
            )
        ));
    }

    /**
     * @return string|null
     */
    public function getParent()
    {
        return DateTimeType::class;
    }
}
