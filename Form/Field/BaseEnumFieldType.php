<?php

namespace Doudou\BaseBundle\Form\Field;

use Doudou\BaseBundle\Enum\AbstractBaseEnum;
use Doudou\BaseBundle\Form\Transformer\EnumTransformer;
use Doctrine\Common\Collections\Collection;
use ReflectionException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class BaseEnumFieldType
 * @package Annuaire\AnnuaireBundle\Form\Field
 */
abstract class BaseEnumFieldType extends AbstractType
{
    /**
     * @var EnumTransformer
     */
    protected $enumTransformer;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var array|null
     */
    protected $choices;


    public function __construct(
        EnumTransformer $enumTransformer,
        TranslatorInterface $translator
    ) {
        $this->translator       = $translator;
        $this->enumTransformer  = $enumTransformer;
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options
     * @throws ReflectionException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'choices' => $this->getChoices(),
            'choices_enum' => null
        ));
        $resolver->setAllowedTypes('choices_enum', array(Collection::class, 'array', 'null'));
    }

    /**
     * Builds the form.
     *
     * This method is called for each type in the hierarchy starting from the
     * top most type. Type extensions can further modify the form.
     *
     * @see FormTypeExtensionInterface::buildForm()
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer($this->enumTransformer);
    }

    /**
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if ($options['choices_enum'] !== null) {
            $choices = array();
            /** @var AbstractBaseEnum $enum */
            foreach ($options['choices_enum'] as $enum) {
                /** @var ChoiceView $choice */
                foreach ($view->vars['choices'] as $key => $choice) {
                    if ($enum->getValue() === $choice->data) {
                        $choices[$key] = $choice;
                    }
                }
            }
            $view->vars = array_replace($view->vars, array(
                'choices' => $choices,
            ));
        }
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return Select2Type::class;
    }

    /**
     * @param Collection|null $enums
     * @return array
     * @throws ReflectionException
     */
    protected function getChoices(?Collection $enums = null): array
    {
        if ($this->choices === null) {
            if ($enums === null) {
                /** @var AbstractBaseEnum $enumClass */
                $enumClass = $this->enumTransformer->getEnumClassName();
                /** @var AbstractBaseEnum[] $enums */
                $enums = $enumClass::getAll();
            } else {
                $enums = $enums->toArray();
            }

            $result = array();

            for ($i = 0, $iMax = count($enums); $i < $iMax; $i++) {
                $name = $this->translator->trans($enums[$i]);
                $result[$name] = $enums[$i]->getValue();
            }
            $this->choices = $result;
        }

        return $this->choices;
    }
}
