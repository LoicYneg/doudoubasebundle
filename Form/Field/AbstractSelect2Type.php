<?php

namespace Doudou\BaseBundle\Form\Field;

use InvalidArgumentException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;

abstract class AbstractSelect2Type extends AbstractType
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if (isset($options['placeholder']) && '' !== $options['placeholder'] && false !== $options['placeholder']) {
            throw new InvalidArgumentException(
                'With Select2, you can\'t use "placeholder" option. Use "attr" => ["data-placeholder" => ...].'
            );
        }
        if (isset($options['attr']['data-placeholder'])) {
            $view->vars['attr']['data-placeholder'] = $this->translator->trans($options['attr']['data-placeholder']);
        } else {
            $view->vars['attr']['data-placeholder'] = $this->translator->trans('action.choice_value');
        }
        if (isset($view->vars['choices']) && $view->vars['choices'] !== null) {
            array_unshift($view->vars['choices'], new ChoiceView(null, '', ''));
        } else {
            $view->vars['choices'] = array(new ChoiceView(null, '', ''));
        }
        if (isset($options['required'])) {
            $view->vars['attr']['required'] = $options['required'];
        }
        $view->vars['attr']['allow_clear'] = $options['allow_clear'];
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined('allow_clear');
        $resolver->setAllowedTypes('allow_clear', 'bool');
        $resolver->setDefaults(array(
            'allow_clear' => true
        ));
    }
}
