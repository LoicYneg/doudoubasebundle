<?php

namespace Doudou\BaseBundle\Form\Field;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class FonctionStatutFieldType
 * @package Formatio\V2Bundle\Form
 */
class Select2Type extends AbstractSelect2Type
{
    public function getParent()
    {
        return ChoiceType::class;
    }
}
