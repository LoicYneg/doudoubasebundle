<?php

declare(strict_types=1);

namespace Doudou\BaseBundle\Form\Field;

use Doudou\BaseBundle\Enum\ConstantEnum;
use Doudou\BaseBundle\Form\Transformer\EnumTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;

final class ConstantEnumFieldType extends BaseEnumFieldType
{
    /**
     * FormeFieldType constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(
        TranslatorInterface $translator
    ) {
        parent::__construct(new EnumTransformer(ConstantEnum::class), $translator);
    }

    /**
     * @return string
     */
    public function getParent(): string
    {
        return ChoiceType::class;
    }

    /**
     * @param OptionsResolver $resolver
     * @throws \ReflectionException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'label' => 'constant.label',
            'required' => true,
            'multiple' => false,
            'placeholder' => false,
        ));
    }
}
