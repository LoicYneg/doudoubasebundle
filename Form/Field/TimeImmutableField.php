<?php

declare(strict_types=1);

namespace Doudou\BaseBundle\Form\Field;

use Symfony\Component\Form\Extension\Core\Type\TimeType;

/**
 * Class DateTimeToCheckBoxFieldType
 * @package Doudou\BaseBundle\Form\Field
 */
class TimeImmutableField extends AbstractDateTimeToImmutable
{
    /**
     * @return string
     */
    public function getParent(): string
    {
        return TimeType::class;
    }
}
