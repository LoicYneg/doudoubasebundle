<?php

declare(strict_types=1);

namespace Doudou\BaseBundle\Controller;

use Doudou\BaseBundle\Exception\AjaxExecutionException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Translation\TranslatorInterface;
use Throwable;

/**
 * Class AbstractBaseController
 * @package Doudou\BaseBundle\Controller
 */
class AbstractBaseController extends AbstractController
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * BaseController constructor.
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     */
    public function __construct(
        TranslatorInterface $translator,
        LoggerInterface $logger
    ) {
        $this->translator = $translator;
        $this->logger = $logger;
    }

    /**
     * @param string $message #TranslationKey
     * @param array $parameters
     * @param int|null $pluralizeNumber if is not null transChoice is used instead of simple trans
     * @return void
     */
    protected function addSuccessMessage(
        string $message,
        array $parameters = array(),
        ?int $pluralizeNumber = null
    ): void {
        $this->extendedAddFlash('success', $message, $parameters, $pluralizeNumber);
    }

    /**
     * @param string $message #TranslationKey
     * @param array $parameters
     * @param int|null $pluralizeNumber if is not null transChoice is used instead of simple trans
     * @return void
     */
    protected function addWarningMessage(
        string $message,
        array $parameters = array(),
        ?int $pluralizeNumber = null
    ): void {
        $this->extendedAddFlash('warning', $message, $parameters, $pluralizeNumber);
    }

    /**
     * @param string $message #TranslationKey
     * @param array $parameters
     * @param int|null $pluralizeNumber if is not null transChoice is used instead of simple trans
     * @return void
     */
    protected function addErrorMessage(
        string $message,
        array $parameters = array(),
        ?int $pluralizeNumber = null
    ): void {
        $this->extendedAddFlash('danger', $message, $parameters, $pluralizeNumber);
    }

    /**
     * @param string $type
     * @param string $message
     * @param array $parameters
     * @param int|null $pluralizeNumber
     */
    private function extendedAddFlash(
        string $type,
        string $message,
        array $parameters = array(),
        ?int $pluralizeNumber = null
    ): void {
        if ($pluralizeNumber !== null) {
            $trans = $this->translator->transChoice($message, $pluralizeNumber, $parameters);
        } else {
            $trans = $this->translator->trans($message, $parameters);
        }
        $this->addFlash($type, $trans);
    }

    /**
     * @param Request $request
     * @return bool
     */
    protected function isAjaxRequest(Request $request): bool
    {
        return $request->isXmlHttpRequest();
    }

    /**
     * @param Request $request
     * @param string $controllerMetod
     * @throws RuntimeException
     */
    protected function ajaxRequestRequired(Request $request, string $controllerMetod): void
    {
        if (!$this->isAjaxRequest($request)) {
            throw new RuntimeException($controllerMetod . ' is only available on Ajax.');
        }
    }
}
