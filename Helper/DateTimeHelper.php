<?php

declare(strict_types=1);

namespace Doudou\BaseBundle\Helper;

use DateTimeImmutable;
use DateTimeInterface;

/**
 * Class DateTimeHelper
 * @package App\Helper
 */
final class DateTimeHelper
{
    /**
     * @param DateTimeInterface $dateTime
     * @return string
     */
    public static function formatDate(DateTimeInterface $dateTime): string
    {
        return $dateTime->format('d/m/Y');
    }

    /**
     * @param DateTimeInterface $dateTime
     * @return string
     */
    public static function formatDateTime(DateTimeInterface $dateTime): string
    {
        return $dateTime->format('d/m/Y H:i');
    }

    /**
     * @param DateTimeInterface $dateTime
     * @return string
     */
    public static function formatTime(DateTimeInterface $dateTime): string
    {
        return $dateTime->format('H\Hi');
    }

    /**
     * @param DateTimeInterface $dateTime1
     * @param DateTimeInterface $dateTime2
     * @return int
     */
    public static function diffBetweenDateTimeInDays(DateTimeInterface $dateTime1, DateTimeInterface $dateTime2): int
    {
        return $dateTime1->diff($dateTime2)->days;
    }

    /**
     * @param DateTimeInterface $dateTime
     * @return int
     */
    public static function diffBetweenDateTimeInMinutes(DateTimeInterface $dateTime): ?int
    {
        $standardDate = new DateTimeImmutable('1970-01-01');
        return (int) $dateTime->diff($standardDate)->i;
    }
}
