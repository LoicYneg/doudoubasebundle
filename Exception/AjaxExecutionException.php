<?php

namespace Doudou\BaseBundle\Exception;

use Throwable;

class AjaxExecutionException extends \Exception
{
    /**
     * @var string
     */
    private $htmlResponse;

    public function __construct(string $htmlResponse, Throwable $exception)
    {
        $this->htmlResponse = $htmlResponse;
        parent::__construct($exception->getMessage(), $exception->getCode(), $exception);
    }

    /**
     * @return string
     */
    public function getHtmlResponse(): string
    {
        return $this->htmlResponse;
    }
}
