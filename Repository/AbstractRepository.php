<?php

namespace Doudou\BaseBundle\Repository;

use Doudou\BaseBundle\Utilities\Paginator;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use App\Contract\Manager\PaginatorInterface;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Adapter\DoctrineORMAdapter;

abstract class AbstractRepository extends EntityRepository
{
    public const PAGER_DEFAULT_ITEM_BY_PAGE = 10;

    /**
     * @param Query $query
     * @param int|null $limit
     * @param int|null $page ( default if limit is null and not limit with 0 )
     * @param bool|null $useOutputWalker
     * @return PaginatorInterface
     */
    protected function createPaginator(
        Query $query,
        ?int $limit = null,
        ?int $page = 1,
        ?bool $useOutputWalker = null
    ): PaginatorInterface {
        if ($limit === null) {
            $limit = $this::PAGER_DEFAULT_ITEM_BY_PAGE;
        }
        if ($limit === 0) {
            $arrayAdapter = new ArrayAdapter($query->getResult());
            $pager = new Paginator($arrayAdapter);
            $pager->setMaxPerPage($pager->count());
            return $pager;
        }
        $paginator = new Paginator(new DoctrineORMAdapter($query, true, $useOutputWalker));
        $paginator->setMaxPerPage($limit);
        if ($page !== null) {
            $paginator->setCurrentPage($page);
        }
        return $paginator;
    }
}
