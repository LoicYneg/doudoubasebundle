<?php

namespace Doudou\BaseBundle\Doctrine\Type;

use function call_user_func;
use function implode;
use InvalidArgumentException;
use Doudou\BaseBundle\Enum\AbstractBaseEnum;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use function sprintf;

abstract class BaseEnumType extends Type
{
    public const INTEGER = 'integer';
    public const STRING  = 'string';

    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param array $fieldDeclaration The field declaration.
     * @param AbstractPlatform $platform The currently used database platform.
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        switch ($this->getSqlType()) {
            case self::INTEGER:
                return $platform->getIntegerTypeDeclarationSQL([]);
            case self::STRING:
                return $platform->getVarcharTypeDeclarationSQL([]);
            default:
                throw new InvalidArgumentException('BaseEnumType::getSQLDeclaration() : '
                    . $this->getSqlType() . ' is not a managed type.');
        }
    }

    /**
     * @param string|null $value
     * @param AbstractPlatform $platform
     * @return AbstractBaseEnum
     * @throws InvalidArgumentException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?AbstractBaseEnum
    {
        if ($value === null) {
            return null;
        }

        if (is_numeric($value)) {
            $value = (int) $value;
        }
        $isValid = call_user_func([$this->getEnumClassName(), 'isValidValue'], $value);
        if (! $isValid) {
            throw new InvalidArgumentException(sprintf(
                'The value "%s" is not valid for the enum "%s". Expected one of ["%s"]',
                $value,
                $this->getEnumClassName(),
                implode('", "', call_user_func([$this->getEnumClassName(), 'getAll']))
            ));
        }
        $enumClass = $this->getEnumClassName();
        return $enumClass::byValue($value, false);
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return mixed|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null) {
            return null;
        }

        /** @var AbstractBaseEnum $value */
        return $value->getValue();
    }

    /**
     * @return string
     */
    protected function getSqlType(): string
    {
        return self::INTEGER;
    }

    /**
     * @param AbstractPlatform $platform
     * @return boolean
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }

    abstract protected function getEnumClassName(): string;
}
