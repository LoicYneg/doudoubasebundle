<?php

namespace Doudou\BaseBundle\Contract\Manager;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use JsonSerializable;

interface PaginatorInterface extends Countable, IteratorAggregate, JsonSerializable
{
    /**
     * Returns the current page.
     *
     * @return integer
     */
    public function getCurrentPage();

    /**
     * Returns the number of results.
     *
     * @return integer
     */
    public function getNbResults();

    /**
     * Returns the number of pages.
     *
     * @return integer
     */
    public function getNbPages();

    /**
     * Returns if the number of results is higher than the max per page.
     *
     * @return boolean
     */
    public function haveToPaginate();

    /**
     * Returns whether there is previous page or not.
     *
     * @return boolean
     */
    public function hasPreviousPage();

    /**
     * Returns whether there is next page or not.
     *
     * @return boolean
     */
    public function hasNextPage();

    /**
     * Returns the next page.
     *
     * @return integer
     */
    public function getNextPage();

    /**
     * Implements the \Countable interface.
     *
     * @return integer The number of results.
     */
    public function count();

    /**
     * Implements the \IteratorAggregate interface.
     *
     * @return ArrayIterator instance with the current results.
     */
    public function getIterator();

    /**
     * Implements the \JsonSerializable interface.
     *
     * @return array current page results
     */
    public function jsonSerialize();
}