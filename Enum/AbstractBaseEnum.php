<?php

namespace Doudou\BaseBundle\Enum;

use BadMethodCallException;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Zul3s\EnumPhp\Enum;

abstract class AbstractBaseEnum extends Enum
{
    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->normalize() . '.' . $this->getValue();
    }

    /**
     * @return string
     * @deprecated
     */
    public function getDescription(): string
    {
        throw new BadMethodCallException('The getDescription method on '
            . static::class . ' can not be called.');
    }

    /**
     * @return string
     */
    public function normalize(): string
    {
        $converter = new CamelCaseToSnakeCaseNameConverter();
        $className = static::class;
        $array = explode('\\', $className);
        /** @var string $propertyName */
        $propertyName = array_pop($array);
        return $converter->normalize($propertyName);
    }
}
