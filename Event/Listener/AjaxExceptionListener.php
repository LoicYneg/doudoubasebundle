<?php

namespace Doudou\BaseBundle\Event\Listener;

use Doudou\BaseBundle\Exception\AjaxExecutionException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class AjaxExceptionListener
{
    /**
     * @var string
     */
    private $env;

    public function __construct(string $env)
    {
        $this->env = $env;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event): void
    {
        // You get the exception object from the received event
        $exception = $event->getException();
        if (!$exception instanceof AjaxExecutionException) {
            return;
        }

        if ($this->env === 'dev' || $this->env === 'test') {
            return;
        }
        $code = $exception->getCode() !== 0 ? $exception->getCode() : 500;
        $response = new Response($exception->getHtmlResponse(), $code);
        $event->setResponse($response);
    }
}
